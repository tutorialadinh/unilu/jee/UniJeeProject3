function scrollToBottom() {
    $('.ui-datatable-scrollable-body').scrollTop(100000)
}

function hideDialogOnSuccess(args, dialogWidgetVar) {
    if (args && !args.validationFailed) {
        PF(dialogWidgetVar).hide();
    }
}