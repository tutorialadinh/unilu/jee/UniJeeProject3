package lu.uni.example.model.composite;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.*;
import java.io.Serializable;

@Embeddable
public class MoviePK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @NotEmpty(message = "Please enter Movie's title")
    @Size(max = 255, message = "No more than 255 characters")
    private String title;

    @Getter
    @Setter
    @NotNull(message = "Please enter Movie's year")
    @Min(1895)
    @Max(1000000000)
    private int year;

    /**
     * Default constructor : need to be declared
     */
    public MoviePK() {
    }

    public MoviePK(String title, int year) {
        this.title = title;
        this.year = year;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.title.hashCode();
        hash = hash * prime + this.year;
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof MoviePK)) {
            return false;
        }
        MoviePK castOther = (MoviePK) other;
        return
                this.title.equals(castOther.title)
                        && (this.year == castOther.year);
    }
}
