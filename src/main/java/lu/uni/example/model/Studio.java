package lu.uni.example.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity(name = "Studio")
@Table(name = "Studio")
public class Studio implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @Id
    @Column(name = "name")
    @NotEmpty(message = "Please a Studio's name")
    @Size(max = 30, message = "No more than 30 characters")
    private String name;

    @Getter
    @Setter
    @Column(name = "address")
    @NotEmpty(message = "Please a Studio's address")
    @Size(max = 255, message = "No more than 255 characters")
    private String address;

    @Getter
    @Setter
    //bi-directional many-to-one association to MovieExec
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "presCertN")
    private MovieExec movieExec;
}
