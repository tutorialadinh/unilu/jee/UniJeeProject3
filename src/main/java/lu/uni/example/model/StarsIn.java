package lu.uni.example.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity(name = "StarsIn")
@Table(name = "StarsIn")
public class StarsIn implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @Id
    private int id;

    @Getter
    @Setter
    @Column(name = "name")
    @NotEmpty(message = "Please enter a Star's name")
    @Size(max = 30, message = "No more than 30 characters")
    private String name;

    @Getter
    @Setter
    //bi-directional many-to-one association to Movie
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "title", referencedColumnName = "title"),
            @JoinColumn(name = "year", referencedColumnName = "year")
    })
    private Movie movie;
}
