package lu.uni.example.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "MovieExec")
@Table(name = "MovieExec")
public class MovieExec implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @Id
    @Column(name = "certN")
    @NotNull(message = "Please enter a certification Nbr.")
    @Min(1)
    @Max(1000000000)
    private int certN;

    @Getter
    @Setter
    @Column(name = "name")
    @NotEmpty(message = "Please a Producer's name")
    @Size(max = 30, message = "No more than 30 characters")
    private String name;

    @Getter
    @Setter
    @Column(name = "address")
    @NotEmpty(message = "Please a Producer's address")
    @Size(max = 255, message = "No more than 255 characters")
    private String address;

    @Getter
    @Setter
    @Column(name = "netWorth")
    @NotNull(message = "Please a Producer's net worth")
    @Min(1)
    @Max(1000000000)
    private int netWorth;

    @Getter
    @Setter
    @OneToMany(mappedBy = "movieExec")  // Name of field in Studio entity
    private List<Studio> listOfStudios;

    @Getter
    @Setter
    @OneToMany(mappedBy = "movieExec")
    private List<Movie> listOfMovies;
}
