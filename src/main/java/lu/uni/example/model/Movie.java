package lu.uni.example.model;

import lombok.Getter;
import lombok.Setter;
import lu.uni.example.model.composite.MoviePK;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Movie")
@Table(name = "Movie")
public class Movie implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @EmbeddedId
    private MoviePK pk;

    @Getter
    @Setter
    @Column(name = "length")
    @NotNull(message = "Please precise Movie's length")
    @Min(1)
    @Max(150)
    private int length;

    @Getter
    @Setter
    @Column(name = "inColor")
    @NotEmpty(message = "Please precise Movie's color category")
    @Size(max = 1, message = "No more than 1 character")
    private String inColor;

    @Getter
    @Setter
    @Column(name = "studioName")
    @NotEmpty(message = "Please enter Movie's Studio name")
    @Size(max = 255, message = "No more than 255 characters")
    private String studioName;

    @Getter
    @Setter
    //bi-directional many-to-one association to MovieExec
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "producerCertN")
    private MovieExec movieExec;

    @Getter
    @Setter
    //bi-directional many-to-one association to StarsIn
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "movie", cascade = CascadeType.ALL)
    private List<StarsIn> starsIns;

    /**
     * Default constructor used when INSERT new record, we must instantiate
     * <p>
     * - MoviePK
     * - MovieExec
     * - List<StarsIn>
     */
    public Movie() {
        this.pk = new MoviePK();
        this.movieExec = new MovieExec();
        this.starsIns = new ArrayList<>();
    }


    // =================================================================================================================================

    /*
     * Method used for resolving the JPA OneToMany persist issue
     * https://stackoverflow.com/questions/38679857/jpa-onetomany-persist-with-cascadetype-all-doesnt-persist-child
     */

    public void addStarsIn(StarsIn starsIn) {
        starsIn.setMovie(this);
        getStarsIns().add(starsIn);
    }

    public void removeStarsIn(StarsIn starsIn) {
        starsIn.setMovie(null);
        getStarsIns().remove(starsIn);
    }
}
