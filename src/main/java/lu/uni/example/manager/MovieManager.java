package lu.uni.example.manager;

import lu.uni.example.model.Movie;
import lu.uni.example.model.StarsIn;
import lu.uni.example.model.composite.MoviePK;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class MovieManager {
    private static final Logger logger = Logger.getLogger(MovieManager.class);

    @PersistenceContext(unitName = "JPA-CDI-Example")
    private EntityManager em;

    public List<Movie> loadAllMovies() {
        return this.em.createQuery("SELECT m FROM Movie m", Movie.class).getResultList();
    }

    public List<Movie> loadAllMoviesByYear(Object value) {
        return this.em.createQuery("SELECT m FROM Movie m WHERE m.pk.year = :selectedYear", Movie.class).setParameter("selectedYear", Integer.parseInt(value.toString().trim())).getResultList();
    }

    public Movie getMovieDetails(String title, int year) {
        MoviePK pk = new MoviePK(title, year);
        return this.em.find(Movie.class, pk);
    }

    public List<StarsIn> getStarLastRecord() {
        return this.em.createQuery("SELECT s FROM StarsIn s ORDER BY s.id DESC", StarsIn.class).setMaxResults(1).getResultList();
    }

    public void insertMovie(Movie movie, int lastStarId) {
        Movie newMovie = new Movie();
        newMovie.setPk(new MoviePK(movie.getPk().getTitle(), movie.getPk().getYear()));

        newMovie.setLength(movie.getLength());
        newMovie.setInColor(movie.getInColor());
        newMovie.setStudioName(movie.getStudioName());

        newMovie.getMovieExec().setName(movie.getMovieExec().getName());
        newMovie.getMovieExec().setAddress(movie.getMovieExec().getAddress());
        newMovie.getMovieExec().setCertN(movie.getMovieExec().getCertN());
        newMovie.getMovieExec().setNetWorth(movie.getMovieExec().getNetWorth());

        if (!(movie.getStarsIns().isEmpty())) {
            logger.info("listOfStarsToAdd NOT EMPTY");
            newMovie.setStarsIns(new ArrayList<>());

            for (StarsIn s : movie.getStarsIns()) {
                lastStarId++;
                s.setId(lastStarId);
                newMovie.addStarsIn(s);
            }

            for (StarsIn s : newMovie.getStarsIns()) {
                logger.info("INSERT Star id : " + s.getId());
                logger.info("INSERT Star Movie title : " + s.getMovie().getPk().getTitle());
                logger.info("INSERT Star Movie year : " + s.getMovie().getPk().getYear());
                logger.info("INSERT Star name : " + s.getName());
            }
        }

        this.em.merge(newMovie);
    }
}
