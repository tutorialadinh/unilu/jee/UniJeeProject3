package lu.uni.example.bean;

import lombok.Getter;
import lombok.Setter;
import lu.uni.example.manager.MovieManager;
import lu.uni.example.model.Movie;
import lu.uni.example.model.StarsIn;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@ManagedBean
@SessionScoped
public class IndexBean implements Serializable {
    private static final Logger logger = Logger.getLogger(IndexBean.class);

    @Getter
    @Setter
    private Map<String, String> availableYearItems;

    @Getter
    @Setter
    private List<Movie> listOfMoviesToDisplay;

    @Getter
    @Setter
    private Movie movieDetails;

    @Getter
    @Setter
    private Movie newMovieToInsert;

    @Getter
    @Setter
    @EJB
    private MovieManager movieManager;


    /**
     * Method executed at startup
     */
    @PostConstruct
    public void init() {
        // Init variable
        this.newMovieToInsert = new Movie();

        // List all Movies
        List<Movie> listOfMovies = this.movieManager.loadAllMovies();

        // Populate List of sorted years
        this.availableYearItems = new TreeMap<>();

        for (Movie m : listOfMovies) {
            this.availableYearItems.put(String.valueOf(m.getPk().getYear()), String.valueOf(m.getPk().getYear()));
        }
    }


    // =============================================================================================================================

    public void getListMoviesBasedOnYear(ValueChangeEvent e) {
        if (e.getNewValue() != null) {
            if (e.getNewValue().toString().trim().equals("ALL")) {
                this.listOfMoviesToDisplay = this.movieManager.loadAllMovies();
            } else {
                this.listOfMoviesToDisplay = this.movieManager.loadAllMoviesByYear(e.getNewValue());
            }
        }
    }

    public void showMovieDetails(String title, int year) {
        movieDetails = this.movieManager.getMovieDetails(title, year);
    }

    public void initListOfStarsToAdd() {
        this.newMovieToInsert.setStarsIns(new ArrayList<>());
    }

    public void addInputTextForStars() {
        this.newMovieToInsert.addStarsIn(new StarsIn());
    }

    public void removeInputTextForStars(StarsIn star) {
        this.newMovieToInsert.removeStarsIn(star);
    }

    public void insertNewMovie() {
        int lastStarId = this.movieManager.getStarLastRecord().get(0).getId();
        this.movieManager.insertMovie(newMovieToInsert, lastStarId);
    }
}